#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

// define common structure to be exchanged

struct vehicle {
   char    name[5];
   double  speed;
   int     ID;
};

// union to share memory for the struct
typedef union {
  vehicle data; 
  char data_buffer[sizeof( vehicle )];
} vehicle_msg;

void printVehicle ( vehicle* v ) { 

	struct vehicle vv;

	vv = *v; // dereference

    printf("The vehicle is %s\n",vv.name);
    printf("The speed   is %f\n",vv.speed);
    printf("The ID      is %d\n",vv.ID);

    return;

}