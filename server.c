// Server script: open a socket on LOCALHOST:port number (by argument)
// In order to send a message consisting in a common C-Struct, to another socket (by argument)

//  #######################################################
//  #  _______   __  _____ ______ _______ _ ____   _____  #
//  #         \ |  |   ___|   ___|__   __| |    \ |       #
//  #          \|  |  __| |  __|    | |  | |     \|       #
//  #       |\     | |____| |       | |  | |   |\         #
//  #  _____| \____| _____|_|       |_|  |_|___| \______  #
//  #                                                     #
//  #######################################################

// Coded by Giammarco Valenti
// /// KEEP IT SIMPLE! //

// It can be compiled both as C and C++

// Based on the tutorial:
// http://www.bogotobogo.com/cplusplus/sockets_server_client.php

// Material on the listen function and the accept functions is at:
// https://stackoverflow.com/questions/31114477/c-server-side-not-blocking-on-listen

// common struct message part:
// https://stackoverflow.com/questions/23966080/sending-struct-over-udp-c

// Instructions can be found in the README file.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>

#include <netinet/in.h>
#include <arpa/inet.h>

#include "commonstruct.h" // The header containing the common C-Struct



void error(const char *msg)
{
    perror(msg);
    exit(1);
} // throwing error function...

int main(int argc, char *argv[])
{


    // ~~~~~~ define instance of the common struct
    struct vehicle velopede;
    struct vehicle* vvp;

    // ~~~~~~ define memory allocation through a UNION
    vehicle_msg messaggio;


    // ~~~~~~ define example of struct to send
    vvp = &velopede;

    vvp->ID = 42;

    vvp->speed = 30.1;

    vvp->name[0] = 'L';
    vvp->name[1] = 'A';
    vvp->name[2] = 'L';
    vvp->name[3] = 'L';
    vvp->name[4] = 'E';



    // ~~~~~~ write the messagge through the union
    messaggio.data = velopede;
    

    // ~~~~~~ networking part


    int sockfd, newsockfd, portno, portno2;   // socket descriptor , server2 socket descriptor(from listen()) , port number
    // Note that socket descriptor is different than socket structure (serv_addr/cli_addr)

    int flag = MSG_WAITALL;                   // Flags for send and receive functions (I still have to investigate it)

    socklen_t serv2len;                       // socket related variable: socket lenght

    struct sockaddr_in serv_addr, serv2_addr; // Client and server socket addresses

    int n;

    if (argc < 3) {
         fprintf(stderr,"ERROR, no ports provided\n"); // if no arguments passed throw error
         exit(1);
    }

    // create a socket
    // socket(int domain, int type, int protocol)

    sockfd =  socket( AF_INET, SOCK_DGRAM, 0 ); // Used to create a new socket, returns a file descriptor for the socket or -1 on error.

    // Arguments list
    // domain: the protocol family of socket being requested:     AF_INET is IPv4!
    // type: the type of socket within that family:           SOCK_DGRAM is UDP/IP!
    // and the protocol.

    if (sockfd < 0) 
        error("ERROR opening socket"); // check if the socket is open or not

    //std::cout << "NO WAY THIS IS THE SHRIMP!" << std::endl; // socket opened feedback to user

    // clear address structure
    bzero( (char *) &serv_addr, sizeof(serv_addr) ); // I think it is kind of a RESET for the address structure of server

    portno = atoi(argv[1]); // riuko mATOI! -> converts string into integer fopr the port number!

    // setup the host_addr structure for use in bind call 
    // server byte order
    serv_addr.sin_family      = AF_INET; // The family in the structure serv_addr.... (server address) 

    // automatically filled with current host's IP address
    serv_addr.sin_addr.s_addr = INADDR_ANY; // It is 0, and it represents all possible IPs (as i understood)

    // convert short integer value for port must be converted into network byte order
    serv_addr.sin_port = htons(portno); // IT CONVERTS THE ORDER OF THE BYTES... BECAUSE OF THE INTEL CONVENTION: LOOK AT:
    // https://beej.us/guide/bgnet/html/multi/htonsman.html

    // bind(int fd, struct sockaddr *local_addr, socklen_t addr_length) // Bind is the operation to associate a socket to a port!
    // bind() passes file descriptor, the address structure, 
    // and the length of the address structure
    // This bind() call will bind  the socket to the current IP address on port, portno
    if (bind(sockfd, (struct sockaddr *) &serv_addr, // Assign directly the pointer: Note that this is in the if because the call itself binds the socket
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding"); // check bindings


    // ~~~~ Create server2 socket address struct ~~~~~
    portno2                    = atoi(argv[2]);
    serv2_addr.sin_family      = AF_INET;   
    serv2_addr.sin_addr.s_addr = INADDR_ANY; 
    serv2_addr.sin_port        = htons(portno2);
    serv2len                   = sizeof(serv2_addr);


    // use a UNION: messaggio.data_buffer is the pointer to the data_buffer[] array of memory shared with the data into the union "vehicle_msg"
    if(sendto( sockfd , messaggio.data_buffer , sizeof( vehicle_msg ) , flag , (struct sockaddr *) &serv2_addr , serv2len ) < 0) 
            error("Error in sending");

    // sendto( ... , Pointer to structure to send , size of structure , ... , ... , ... )
    // reference: http://pubs.opengroup.org/onlinepubs/007904875/functions/sendto.html

    printVehicle( &messaggio.data );            // check the structure after the message
    printf("Structure sent\n" );

    close(sockfd);
    return 0; 

}












